<?php

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array());

require_once 'classes/DB.php';
$database = DB::getDBConnection();

if (!isset($_POST['email'])) {
    $template = $twig->render('newUser.html', array());
} else {
    $error['error_code'] = 1;
    $error['msg'] = "Du må fylle ut alle felter";

    $success['success_code'] = 0;
    $success['msg'] = "Brukeren har blitt opprettet";

    if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['firstname']) && isset($_POST['surname'])) {
        $error['error_code'] = 0;
        
        if(!preg_match("/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/",$_POST['email'])) {
            $error['error_code'] = 1;
            $error['msg'] = "Ugyldig e-post addresse";
        } else if(strlen($_POST['password'])<8) {
            $error['error_code'] = 1;
            $error['msg'] = "Passord må ha minst 8 tegn";
        } else if(strlen($_POST['firstname'])<1) {
            $error['error_code'] = 1;
            $error['msg'] = "Fyll ut fornavn";
        } else if (strlen($_POST['surname'])<1) {
            $error['error_code'] = 1;
            $error['msg'] = "Fyll ut etternavn";
        }
    }

    if($error['error_code'] === 1) {
        $template = $twig->render('newUser.html', $error);
    } else {
        $query = $database->prepare('INSERT INTO user (uname, pwd, firstName, lastName) VALUES(?, ?, ?, ?)');
        $query->execute(array($_POST['email'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST['firstname'], $_POST['surname']));

        if ($query->rowCount()==1) {
            $success['success_code'] = 1;
            $template = $twig->render('newUser.html', $success);
        } else {
            $error['error_code'] = 1;
            $error['msg'] = "Kunne ikke lagre bruker";
            $template = $twig->render('newUser.html', $error);
        }
    }
}

echo $template;