import { LitElement, html, css } from "../../node_modules/lit-element/lit-element.js";

class EditUser extends LitElement {
  static get properties() {
    return {
      user: { type: Object}
    };
  }

  // din kode her
    constructor() {
        super();  
    }
    
    render() {
        return html`
            <form method="post" id="user-form-edit">
                <label for="email">E-post</label>
                <input type="email" id="email" name="uname" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Ugyldig e-post adresse" @input="${this.getEmail}" value="${this.user.uname}">
                <br><br>

                <label for="old_password">Eksisterende passord</label>
                <input type="password" id="old_password" name="oldpwd" pattern=".{8,}" @input="${this.getOldPassword}" title="Minst 8 tegn">
                <br><br>

                <label for="password">Nytt passord</label>
                <input type="password" id="password" name="pwd" pattern=".{8,}" @input="${this.getPassword}" title="Minst 8 tegn">
                <br><br>

                <label for="firstname">Fornavn</label>
                <input type="text" id="firstname" name="firstName" required @input="${this.getFirstName}" value="${this.user.firstName}"><br><br>

                <label for="surname">Etternavn</label>
                <input type="text" id="surname" name="lastName" required @input="${this.getLastName}" value="${this.user.lastName}">
                <br><br>

                <button id="submit" @click="${this.submitForm}">Oppdater bruker</button>

                <div class="" id="message"></div>
            </form>
            `;   
    }
    
    getEmail(e) {
        this.user.uname = e.target.value;
    }
    
    getOldPassword(e) {
        this.user.oldPwd = e.target.value;
    }
    
    getPassword(e) {
        this.user.pwd = e.target.value;
    }
    
    getFirstName(e) {
        this.user.firstName = e.target.value;
    }
    
    getLastName(e) {
        this.user.lastName = e.target.value;
    }
    
    
    submitForm(event) {
        event.preventDefault();
        
        const formData = new FormData();
        
        Object.keys(this.user).forEach(key => {
            formData.append(key, this.user[key])
        })
        
        fetch('api/updateUser.php', {
            method: "POST",
            body: formData
        }).then(response => { 
            return response.json(); 
        }).then(data => {
            if(data) {
                const message = this.shadowRoot.getElementById('message');
                
                if(data.status === "success") {
                    message.classList.add('green');
                    message.classList.remove('red');
                    message.innerHTML = `
                    <style>
                        .green { color: green; font-weight: bold; }
                    </style>
                    <span class='green'>Brukern ble oppdatert</span>`;
                } else {
                    message.classList.add('red');
                    message.classList.remove('green');
                    message.innerHTML = `
                    <style>
                        .red { color: red; font-weight: bold; }
                    </style>
                    <span class='red'>${data.msg}</span>`;
                }
                setTimeout(()=> {
                    message.innerHTML = '';
                }, 4000);
            }
        })
    }

}
customElements.define('edit-user', EditUser);
