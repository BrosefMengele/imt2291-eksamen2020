window.id = 0;

function getUsers() {
    fetch('api/fetchUsers.php').then(response => {
        return response.json();
    }).then(users => {
        const userList = document.querySelector('ul.user-list');
        userList.innerHTML = "";
        
        users.forEach(user => {
            const userListElement = document.createElement ('li');
            userListElement.setAttribute("data-id", user.uid);
            userListElement.innerHTML = `${user.uid}: <strong>${user.uname}</strong><br>`;
            userListElement.innerHTML += `${user.firstName} ${user.lastName}<br><br>`;
            userList.appendChild(userListElement);
        })
        
        userList.addEventListener("click", click => {
            click.path.find(element => {
                if(element.tagName == "LI") {
                    editUser(element.dataset.id);
                }
            })
        })
    })
}

getUsers();

function editUser(id) {
    const userForm = document.querySelector('.user-form');
    if(id !== 0) {
        userForm.style.display = "inline-block";
        
        fetch(`api/fetchUser.php?id=${id}`).then(response => {
            return response.json();
        }).then(user => {
            window.id = user.uid;
            
            document.getElementById('email').value = user.uname;
            document.getElementById('firstname').value = user.firstName;
            document.getElementById('surname').value = user.lastName;
            console.log(user);
        })
    }
}

document.getElementById('submit').addEventListener("click", event => {
    event.preventDefault()
    
    if(window.id !== 0) {
        const form = document.getElementById('user-form-edit')
        const formData = new FormData(form);
        formData.append('uid', window.id);
        
        fetch('api/updateUser.php', {
            method: 'POST',
            body: formData
        }).then(response => {
            return response.json();
        }).then(data => {
            if(data) {
                const message = document.getElementById('message');
                
                if(data.status === "success") {
                    message.classList.add('green');
                    message.classList.remove('red');
                    message.innerHTML = "Brukern ble oppdatert";
                } else {
                    message.classList.add('red');
                    message.classList.remove('green');
                    message.innerHTML = data.msg;
                }
                setTimeout(()=> {
                    message.innerHTML = '';
                }, 2000);
            }
        });
    }
});